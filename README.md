# HZy combination framework

## How to run (on lxplus)

### Check out the packages

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/clcheng/hzycombination.git
```
Make sure all folders in submodules are not empty.

### For the first time (need a compilation)

```
source compile.sh
```

### For the future time
```
source setup.sh
```