#!/bin/bash
# This is not to compile hh_combination_fw
# But a wrapper to compile submodules (Rui Zhang)

# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf "${GREEN}\n
==================================
| Sourcing   source setup.sh  ...
==================================${NC}\n"
if [[ -z $1 ]]; then
    source setup.sh
else
    source setup.sh $1
fi

printf "${GREEN}\n
==============================================
| Compiling submodules/quickstats ...
==============================================${NC}\n"
quickstats add_macro -f -i ${DIR}/extensions/CMSSWCore
quickstats add_macro -f -i ${DIR}/extensions/ATLASSWCore
quickstats compile