#ifndef __ATLASSWCore_CXX__
#define __ATLASSWCore_CXX__

#include "HggBernstein.cxx"

#include "TObject.h"
class ATLASSWCore: public TObject {
    public:
    protected:
    private:
        ClassDef(ATLASSWCore, 1)
    };

ClassImp(ATLASSWCore)
    
    
#ifdef __CINT__

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class HggBernstein+;
    
#endif
    
    
#endif
