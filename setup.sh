#!/bin/bash

if [ "$#" -ge 1 ];
then
	EnvironmentName=$1
else
	EnvironmentName="default"
fi

# more stack memory
ulimit -S -s unlimited

# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

if [ "$EnvironmentName" = "default" ]; #Default is python3
then
    setupATLAS
    source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
	export PATH=${DIR}/submodules/quickstats/bin:${PATH}
	export PYTHONPATH=${DIR}/submodules/quickstats:${PYTHONPATH}
elif [[ "$EnvironmentName" = "nightly" ]];
then
setupATLAS
source /cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-centos7-gcc11-opt/setup.sh
	export PATH=${DIR}/submodules/quickstats/bin:${PATH}
	export PYTHONPATH=${DIR}/submodules/quickstats:${PYTHONPATH}
elif [[ "$EnvironmentName" = "102a" ]];
then
setupATLAS
source /cvmfs/sft.cern.ch/lcg/views/LCG_102a/x86_64-centos7-gcc11-opt/setup.sh
	export PATH=${DIR}/submodules/quickstats/bin:${PATH}
	export PYTHONPATH=${DIR}/submodules/quickstats:${PYTHONPATH}
fi